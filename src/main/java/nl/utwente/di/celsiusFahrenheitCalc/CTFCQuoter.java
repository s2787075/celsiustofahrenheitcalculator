package nl.utwente.di.celsiusFahrenheitCalc;

public class CTFCQuoter {
    public double getDegreeTranslation(String celsiusDegree) {
        double converted = Double.parseDouble(celsiusDegree);
        return converted * 1.8 + 32.0;
    }
}
