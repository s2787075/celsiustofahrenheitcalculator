package nl.utwente.di.celsiusFahrenheitCalc;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets a Celsius degree as input and returns the degrees in Fahrenheit.
 */

public class CTFCServlet extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CTFCQuoter quoter;
	
    public void init() throws ServletException {
        quoter = new CTFCQuoter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Celsius to Fahrenheit calculator";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius degree: " +
                   request.getParameter("degree") + "\n" +
                "  <P>Fahrenheit degree: " +
                   Double.toString(quoter.getDegreeTranslation(request.getParameter("celsius"))) +
                "</BODY></HTML>");
  }


}