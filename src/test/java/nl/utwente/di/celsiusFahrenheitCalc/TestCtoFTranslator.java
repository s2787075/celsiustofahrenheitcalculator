package nl.utwente.di.celsiusFahrenheitCalc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestCtoFTranslator {

    @Test
    public void testCtoF1() throws Exception {
        CTFCQuoter quoter = new CTFCQuoter();
        double fahrenheitDegree = quoter.getDegreeTranslation("10");
        Assertions.assertEquals(50, 50, "Celsius to Fahrenheit");
    }
}
